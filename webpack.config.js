module.exports = {
    context: __dirname + "/src",
    entry: {
        background: "./background.js",
        contentscript: "./contentscript.js"
    },
    output: {
        path: __dirname + "/extension/scripts",
        filename: "[name].bundle.js",
        chunkFilename: "[id].bundle.js"
    },
    module: {
      loaders: [
        {test: /\.js$/, loader: 'babel?presets[]=es2015,presets[]=react', exclude: /node_modules/},
        {test: /\.css$/, loader: 'style!css', exclude: /node_modules/}
      ]
    }
}
