import $ from 'jquery'

export default {
  // add button
  addExtensionButton(){
    $('body').prepend('<div class="btn-open-extension">open extension</div>')
    $('body').on('click', '.btn-open-extension', ()=>{
      this.openExtensionBox()
    })
  },

  openExtensionBox(){
    $('#extension-wrapper').show();
  },

  contextMenusListener(){
    chrome.runtime.onMessage.addListener(
      (request, sender, sendResponse) => {
        switch(request.type){
           case 'OPEN_EXTENSION_BOX':
           this.openExtensionBox()
           break;
        }
    });
  }


}
